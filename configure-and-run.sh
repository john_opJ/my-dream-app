#!/bin/sh
set -e
set -u

# echo $APPCONFIG

# # Load the environment specific settings from the environment variables
# # that were put in by docker.
# echo $APPCONFIG > /var/www/html/assets/config.json

exec nginx "$@"
