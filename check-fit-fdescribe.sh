#!/bin/sh
if grep -R --include="*.spec.ts" --exclude-dir="node_modules" "fit(\|fdescribe("; then
    echo FAIL: fit or fdescribe found. Change it back to it/describe
    exit 1
else
    exit 0
fi
